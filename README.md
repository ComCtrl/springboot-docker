# SpringBoot、docker、Jenkins整合

完整的目录结构

![1536896001927](images/1536896001927.png)



## 新建 SpringBoot项目

建一个最简单的 springboot项目，访问 http://localhost:8080/hello 

```java
@RestController
public class HelloController {
    @GetMapping("/hello")
    public String hello(){
        return "hello";
    }
}
```



## Dockerfile

Dockerfile 文件用来说明如何来构建镜像

* FROM ，表示使用 Jdk8 环境 为基础镜像，如果镜像不是本地的会从 DockerHub 进行下载
* VOLUME ，VOLUME 指向了一个`/tmp`的目录，由于 Spring Boot 使用内置的Tomcat容器，Tomcat 默认使用`/tmp`作为工作目录。这个命令的效果是：在宿主机的`/var/lib/docker`目录下创建一个临时文件并把它链接到容器中的`/tmp`目录
* ADD ，拷贝文件并且重命名
* ENTRYPOINT ，为了缩短 Tomcat 的启动时间，添加`java.security.egd`的系统属性指向`/dev/urandom`作为 ENTRYPOINT

```
FROM java:8
VOLUME /tmp
ADD springboot-docker-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
```



## 引入 docker maven plugin

```xml
<properties>
    <docker.image.prefix>springboot</docker.image.prefix>
</properties>

<build>
    <plugins>
        <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
        </plugin>

        <!-- Docker maven plugin -->
        <plugin>
            <groupId>com.spotify</groupId>
            <artifactId>docker-maven-plugin</artifactId>
            <version>1.1.0</version>
            <configuration>
                <imageName>${docker.image.prefix}/${project.artifactId}</imageName>
                <!-- Dockerfile 文件所在路径 -->
                <dockerDirectory>.</dockerDirectory>
                <resources>
                    <resource>
                        <targetPath>/</targetPath>
                        <directory>${project.build.directory}</directory>
                        <include>${project.build.finalName}.jar</include>
                    </resource>
                </resources>
            </configuration>
        </plugin>
        <!-- Docker maven plugin -->
    </plugins>
</build>
```



## 构建镜像

1. 确保配置好以下环境
   * docker
   * maven



2. git clone或者将项目 copy 到虚拟机中

3. 解压项目

   ```
   [root@localhost ~]# unzip -d springboot-docker springboot-docker.zip
   ```

4. 进入项目，执行构建

   ```
   [root@localhost springboot-docker]# mvn package docker:build
   ```

5. 构建完毕后查看所有镜像

   ```
   [root@localhost springboot-docker]# docker images
   REPOSITORY                          TAG                 IMAGE ID            CREATED             SIZE
   springboot/springboot-docker        latest              1da89aac3b86        52 seconds ago      658 MB
   docker.io/java                      8                   d23bdf5b1b1b        20 months ago       643 MB
   ```

6. 运行镜像查看结果

   ```
   [root@localhost springboot-docker]# docker run -d -p 8080:8080 springboot/springboot-docker
   f5db82e51956064041d75577e4402a52f940dd8dc50a6c4f882628e1ba4dc342
   ```

7. 查看日志有没有报错

   ```
   [root@localhost springboot-docker]# docker ps # 查看正在运行的容器
   CONTAINER ID        IMAGE                          COMMAND                  CREATED             STATUS              PORTS                    NAMES
   f5db82e51956        springboot/springboot-docker   "java -Djava.secur..."   37 seconds ago      Up 37 seconds       0.0.0.0:8080->8080/tcp   adoring_bhaskara
   
   [root@localhost springboot-docker]# docker logs f5db82e51956 # 查看容器日志
   ```

8. curl 访问查看结果

   ```
   [root@localhost springboot-docker]# curl http://localhost:8080/hello
   hello
   ```

9. 宿主机访问查看结果

   ![1536820382671](images/1536820382671.png)



## 使用Jenkins自动构建

1. 配置公钥

   略

2. Jenkins 中使用流水线构建

   ![1536896175685](images/1536896175685.png)

3. 输入脚本

   ![1536896209954](images/1536896209954.png)

4. groovy脚本

   ```groovy
   #!groovy
   pipeline{
       agent any
       //定义仓库地址
       environment {
           REPOSITORY = "git@gitee.com:ComCtrl/springboot-docker.git"
           project = "springboot-docker" //项目名称
           image_name = "springboot/springboot-docker" //镜像名称
       }
   
       stages {
   
           stage('获取代码'){
               steps {
                   echo "从 git:${REPOSITORY} 拉取代码"
                   //清空当前目录
                   deleteDir()
                   //拉取代码    
                   git "${REPOSITORY}"
               }
           }
    
   
           stage('构建镜像'){
               steps {
                   echo "开始构建"
                   //构建镜像
                   sh 'mvn package -Dmaven.test.skip=true docker:build'
               }
           }
   
           stage('停止原有服务'){
               steps{
                   script {
                       try {
                           echo "停止服务"
                           sh 'docker stop ${project}'
                       } catch(ex) {
   
                       }
                   }
               }
           }
   
           stage("启动服务"){
               steps {
                   script {
                       try {
                           echo "启动服务"
                           // -v /etc/localtime:/etc/localtime:ro 同步时间
                           sh 'docker run -v /etc/localtime:/etc/localtime:ro --name ${project} -d -p 8081:8081 ${image_name}'
                       } catch(ex) {
                           sh 'docker start ${project}'
                       }
                   }
               }
           }        
       }
   }
   ```


